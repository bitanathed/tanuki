**Update:** This project was one of the winners in the hackathon it was developed for. Check out `Summary.ipynb` notebook for an annotated methodology with training code and inference code which formed the core of our idea. This repository also now contains updated weights to support the notebook.

Original readme below:

---
# Tanuki
_**Developed for Gitlab Hackathon, not intended to be used in production applications**_

🦊 *A Tanuki is another name for a Racoon Dog spirit, whose fun loving nature leads it to make fools out of people. It is also the shape behind the Gitlab logo. We named our project this since we were developing a Gitlab Component for the first time, making absolute fools of ourselves in the process :).* 🐶

#### Template `tanuki-component.yml` depends on the following packages

- bitanathed/py-web-heatmap> To generate the Heatmap and overall attention and relevance report
- bitanathed/node-pally-config> To generate the Accessibility report using Pa11y
- [weights.pth](https://gitlab.com/bitanathed/py-web-heatmap/-/blob/cf62a09eff4aa81c6c9b702ed825b28cc1c65a69/webheatmap/weights.pth)> Model weights permalinked to gitlab repository

#### Generates the Following Badge, along with the below report to integrate into Gitlab Pages or to download directly.

For example, the below artifacts are generated for https://about.gitlab.com/community/heroes/ and can be displayed on the repository as a badge, or can also be pushed downstream to gitlab pages

- ![Badge](https://gitlab.com/api/v4/projects/55295721/jobs/artifacts/main/raw/badge.svg?job=reporting_job) with an overall score
- [This HTML report](https://gitlab.com/api/v4/projects/55295721/jobs/artifacts/main/raw/tanuki_report/index.html?job=reporting_job) with metrics and other details

![Attention and Relevance Heatmap Generation](images/screen1.png "Report Generated for a Gitlab Page on Desktop Device")

## Here's how to interpret the heatmap, which is actually a superimposition of Two heatmaps:
- A Jet colored attention heatmap that predicts which parts of the webpage are most likely to be seen
- A White area heatmap that predicts which areas of the webpage are the most relevant to the user

### What are we trying to accomplish?
An **Intersection over Union (IOU)** over Predicted Attention versus Predicted Relevance on a given webpage for a given device in order to continuously test UI/UX changes with the end goal of reducing bounce and increasing revenues. According to relevant research from [here](https://simplyintel.com/bounce-rate/):

![Bounce Rate Sensitivity](images/morebounce.jpg)

![Heatmap Legend](images/screen2.png)

The CI/CD pipeline runs to finally produces an Overall Score, which is the Geometric mean of the attention and relevance scores. This can be used to produce badges or otherwise as a North Star Metric for UX testing while deploying. Below is an example of such a score.

![Reported Scores](images/screen3.png "Report Generated for the same Gitlab Page")


## Why this project - UI/UX testing in its current form can't capture Human Attention 

While automated tools for UI/UX testing in CI/CD pipelines do an excellent job of testing issues with usability  and accessibility, they are bad at following human behavior to recommend changes to an interface that might be _**functionally usable and robust but not attention grabbing or relevant.**_ 

There is no established ground truth or pattern to what design constitutes an attention grabbing website. Most design is ad hoc with feedback being captured post MVP development, which makes human behavior driven development and testing a very expensive proposition.

## The Question - Is AI Generated Human Attention Modelling truly Possible?

❌ Simple answer, not through multimodal LLMs like ChatGPT or Gemini yet. We tried identifying human attention elements on interfaces using all major multimodal LLMs and while these have good image understanding abilities, the ability to understand a human's thought process and translate it to an easily understandable heatmap on a webpage is beyond them. **For Now.**

🟢 Instead the question we sought to answer is, can an AI model **mimic** human attention. This can be done through training on existing datasets like [Rico](http://www.interactionmining.org/rico.html#quick-downloads) and [FiWi](https://www-users.cse.umn.edu/~qzhao/webpage_saliency.html). It is a relatively robust way to provide a deterministic solution to the problem, without the unreliable temperature of a vision modal LLM

## The Answer - Challenging Traditional CI Pipeline Components

Functionally we divided the component into 3 parts, while integrating AI into the pipeline component directly and using an LLM (Gemini) as an API call. These parts were all in the same stage of the pipeline overall, and the Page URL to be tested, the stage to test in, and the device to simulate were all inputs to the pipeline component

- 1️⃣ Accessibility report for elements with low contrast, hidden states or html spec violations, this provided the groundwork for further analysis. We did this using Pa11y, a commonly used tool implementing A11y testing with the WCAG2AA principles.
- 2️⃣ Pytorch model inference based on screenshots of the website simulating device screen size and user agent (for mobile, tablet or desktop) and heat map generation modelling human attention on the page.
- 3️⃣ Gemini (Google LLM) based summarisation and report creation, publishing the report as a final step t o GitLab Pages.

## What's next for Tanuki - CI/CD Component Mapping Human Attention through AI

👩‍🔬 If given the chance, we'd definitely like to try and experiment some more in order to use better quality Data and Longer Training times for our models for creating individual personas. Imagine a website being individually tailored to you, based on where you'd be most likely to focus your attention, based on data voluntarily supplied by you to improve your browsing experience! 

🐯 There are multiple architectures (Vision Transformers as well as Multimodal LLMs) apart from a ConvNet like Mobilenet that we've used here, that if hardware is no constraint can improve accuracy by an order of magnitude! Presumably a new architecture someday will completely replace frontend developers like us (wouldn't that be neat), and we would be interested in applying new tech to build a more robust Continuous Integration Pipeline that tests an interface just as a human would!
